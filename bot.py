import sys
import telepot
import random
import sched, time
from telepot.delegate import per_chat_id, create_open, pave_event_space
import praw

TOKEN = "297896064:AAFyWwiPWhjuSlJz3QM88zQWeR6RU3qjaa4"
MEME_SUBREDDITS = [ "adviceanimals", "2meirl4meirl", "dankmemes"]
#s = sched.scheduler(time.time, time.sleep)
meme_urls = []
reddit = praw.Reddit(client_id='r9pCrQV8J-7BDQ',
                     client_secret='NeNFtW9UfYyxQACOxSPT95pDeDk',
                     user_agent='ucll_memebot')

# Fills the list meme_urls with URLs of the hottest images on the defined subreddits
def get_memes():
    global meme_urls
    meme_urls = []
    # go through the subreddits and get the hottest imgur posts
    print("Getting Memes...")
    for subreddit in MEME_SUBREDDITS:
        print("Subreddit " + subreddit)
        reddit_sub = reddit.subreddit(subreddit)
        for submission in reddit_sub.hot(limit=20):
            if "imgur" in submission.url or "sli.mg" in submission.url:
                print("Meme found: " + submission.url)
                meme_urls.append(submission.url)
    # run again in 3000 seconds
 #   s.enter(3000, 1, get_memes, ())


class MemeBot(telepot.helper.ChatHandler):
    def __init__(self, *args, **kwargs):
        super(MemeBot, self).__init__(*args, **kwargs)
        self._count = 0

    def on_chat_message(self, msg):
        print("Received message")
        print(msg)
        self._count += 1
        self.sender.sendMessage(self.get_meme())
        if self._count == 3:
            self.sender.sendMessage(self.get_phishing_message())

    def get_meme(self):
        global meme_urls
        return random.choice(meme_urls)

    def get_phishing_message(self):
        return "Like memes? Like our facebook page! http://bit.ly/2jfUXKa"



get_memes()
#s.run()
#Creates a seperate memebot for every user
bot = telepot.DelegatorBot(TOKEN, [
    pave_event_space()(
        per_chat_id(), create_open, MemeBot, timeout=86400
    ),
])
bot.message_loop(run_forever='Listening ...')
